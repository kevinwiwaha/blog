<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Auth;

class EditorMail extends Mailable
{
    use Queueable, SerializesModels;
    public $editor;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($editor)
    {
        $this->editor = $editor;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('example@example.com')->subject("Review Blog")
            ->view('emailEditor')->with([
                'nama' => $this->editor->user->name
            ]);
    }
}
