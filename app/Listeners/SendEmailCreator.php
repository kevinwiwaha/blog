<?php

namespace App\Listeners;

use App\Events\BlogSubmited;
use App\Mail\BlogSubmitedMail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class SendEmailCreator implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  BlogSubmited  $event
     * @return void
     */
    public function handle(BlogSubmited $event)
    {
        Mail::to(($event->blog->user->email))->send(new BlogSubmitedMail($event->blog));
    }
}
