<?php

namespace App\Listeners;

use App\Events\BlogApproved;
use App\Mail\ApprovedMail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class SendEmailApproved implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Handle the event.
     *
     * @param  BlogApproved  $event
     * @return void
     */
    public function handle(BlogApproved $event)
    {


        Mail::to(($event->blog->user->email))->send(new ApprovedMail($event->blog));
    }
}
