<?php

namespace App\Listeners;

use App\Events\BlogSubmited;
use App\Mail\BlogSubmitedMail;
use App\Mail\EditorMail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class SendEmailEditor implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  BlogSubmited  $event
     * @return void
     */
    public function handle(BlogSubmited $event)
    {
        Mail::to('editor@redaksi.com')->send(new EditorMail($event->blog));
    }
}
