<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'BlogController@index');

Auth::routes();
Route::middleware('auth')->group(function () {
    Route::get('/profile', 'BlogController@profile');
    Route::get('/tambah', 'BlogController@create');
    Route::put('/tambah', 'BlogController@store');
    Route::get('/publish/{blog}', 'BlogController@publish');
});
Route::get('/home', 'HomeController@index')->name('home');
